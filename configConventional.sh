#!/bin/bash
npm i -D release-it @release-it/conventional-changelog @commitlint/{cli,config-conventional} husky
echo "module.exports = { extends: ['@commitlint/config-conventional'] };" > commitlint.config.js
