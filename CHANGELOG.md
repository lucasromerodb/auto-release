# [4.0.0](https://gitlab.com/lucasromerodb/auto-release/compare/3.0.0...4.0.0) (2020-11-04)


### Features

* **scope:** new feature ([332fb98](https://gitlab.com/lucasromerodb/auto-release/commit/332fb980fe5e479bc0e353a80f525cd2fbb7872f))


### BREAKING CHANGES

* **scope:** yup it's a new feature

# [3.0.0](https://gitlab.com/lucasromerodb/auto-release/compare/2.0.1...3.0.0) (2020-11-04)


### Features

* **robert:** easter egg ([897a669](https://gitlab.com/lucasromerodb/auto-release/commit/897a66924ac50e2b0e7f2ac131b7e79eb6ad3768))


### BREAKING CHANGES

* **robert:**

## [2.0.1](https://gitlab.com/lucasromerodb/auto-release/compare/2.0.0...2.0.1) (2020-11-04)

# [2.0.0](https://gitlab.com/lucasromerodb/auto-release/compare/1.2.2...2.0.0) (2020-11-04)


### Bug Fixes

* **anyscope:** ok dude ([c5a4bf9](https://gitlab.com/lucasromerodb/auto-release/commit/c5a4bf99fcff8a301211f8693bcc88afbcd9fe5e))


### BREAKING CHANGES

* **anyscope:** entirely app

## [1.2.2](https://gitlab.com/lucasromerodb/auto-release/compare/1.2.1...1.2.2) (2020-11-04)


### Bug Fixes

* **scopie:** are you fixing me ([28224db](https://gitlab.com/lucasromerodb/auto-release/commit/28224dbc95964a0c872f53ba1280efb30f043832))

## [1.2.1](https://gitlab.com/lucasromerodb/auto-release/compare/1.2.0...1.2.1) (2020-11-04)


### Bug Fixes

* **coolscope:** a very cool fix ([83d36a5](https://gitlab.com/lucasromerodb/auto-release/commit/83d36a51576dcae7c50fc361ee108026696eea76))
* **newscope:** a cool fix ([92e8ee2](https://gitlab.com/lucasromerodb/auto-release/commit/92e8ee2e26730bb914f38ebdf563b5e49bc70854))

# [1.2.0](https://gitlab.com/lucasromerodb/auto-release/compare/1.1.0...1.2.0) (2020-11-04)


### Bug Fixes

* **myscope:** bullet fix ([7834744](https://gitlab.com/lucasromerodb/auto-release/commit/783474476f2780d0ff51b963fb1338706b09d6a7))
* **socpe:** another bullet fix ([3c7c235](https://gitlab.com/lucasromerodb/auto-release/commit/3c7c2355cb8f249d31be5099ffa96389d015c985))


### Features

* **otherscope:** bullet feat ([ce31f52](https://gitlab.com/lucasromerodb/auto-release/commit/ce31f52f6ca1281d4ddab730365221acf7e26c15))
* **release:** another title ([99acb02](https://gitlab.com/lucasromerodb/auto-release/commit/99acb02becde99827136a1b69fcf32d3f7a962ae))

# [1.1.0](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-11-04)


### Features

* **package:** release config ([6b94eb3](https://gitlab.com/lucasromerodb/auto-release/commit/6b94eb3f11764a025ac8031f37dada797c90e5bb))
* **package:** semantic release package ([b11289a](https://gitlab.com/lucasromerodb/auto-release/commit/b11289a499811fda2f13f8af30b3b66a286df9c2))
* **release:** welcome back to release it ([62411ed](https://gitlab.com/lucasromerodb/auto-release/commit/62411ed3904a8ba3c5778278b6361d829be79ec5))



## [1.0.13](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-31)



## [1.0.12](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-29)



## [1.0.11](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-29)



## [1.0.10](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-29)



## [1.0.9](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-29)


### Bug Fixes

* readme ([0eddd9c](https://gitlab.com/lucasromerodb/auto-release/commit/0eddd9c650b40b12ae7e46a0f7a69f169cfd360a))
* readme 2 ([bd9a3fb](https://gitlab.com/lucasromerodb/auto-release/commit/bd9a3fbb3446a80a0e7dc105f145d7a8a9e79883))


### Features

* auto changelog config ([39d0f06](https://gitlab.com/lucasromerodb/auto-release/commit/39d0f065c387d2badabad01c4a5ec10135758e0c))
* auto changelog done ([0121b21](https://gitlab.com/lucasromerodb/auto-release/commit/0121b21eb45f964630a2f357a95a7282cb4c6079))


### Performance Improvements

* config fix ([37c359f](https://gitlab.com/lucasromerodb/auto-release/commit/37c359fc5711edb300c119bc687e61b54aa24d0b))



## [1.0.8](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-29)



## [1.0.7](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-29)



## [1.0.6](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-29)



## [1.0.5](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-29)



## [1.0.4](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-29)



## [1.0.3](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-29)



## [1.0.2](https://gitlab.com/lucasromerodb/auto-release/compare/1.0.13...1.1.0) (2020-10-28)

